//
//  ViewController.swift
//  soal1
//
//  Created by Oscar Perdanakusuma Adipati on 27/12/20.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class LoginViewController: InitialParentController {
    @IBOutlet weak var usernameView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    var loginModel : LoginModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        customNavBar.isHidden = true
        setTextFieldColor(tField: txtUsername, color: Constant.sharedIns.color_black)
        setTextFieldColor(tField: txtPassword, color: Constant.sharedIns.color_black)
        setRoundedViewWithBorder(view: usernameView, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_white)
        setRoundedViewWithBorder(view: passwordView, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_white)
        setRoundedButton(button: btnLogin, textColor: Constant.sharedIns.color_white, bgColor: Constant.sharedIns.color_orange_800, isBold: true)
        addTapEventForView(view: btnLogin, action: #selector(loginClicked))
        
        txtUsername.delegate = self
        txtPassword.delegate = self
        setEnableTapGestureOnMainView(isEnable: false)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        CommonUtils.AppUtility.lockOrientation(.portrait)
        //Add responder for keyboard
        textFields.removeAll()
        textFields.append(txtUsername)
        textFields.append(txtPassword)

        for index in 0..<textFields.count{
            CommonUtils.addNextButtonOnKeybaord(pos: index, textFields: textFields)
        }

        CommonUtils.addDoneButtonOnKeyboard(textField: txtPassword)
        deregisterFromKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        CommonUtils.AppUtility.lockOrientation(.all)
    }
    
    override func viewDidLayoutSubviews() {
        //Draw bottom border
        super.viewDidLayoutSubviews()
        DispatchQueue.main.async {
            self.setBottomLineViewForPhone(view: self.txtUsername)
            self.setBottomLineViewForPhone(view: self.txtPassword)
        }
    }
    
    @objc func loginClicked () {
        if(isConnectingInternet()==false){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_internet_error, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            return
        }else if(txtUsername.text?.isReallyEmpty == true){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_input_username, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            return
        }else if(txtPassword.text?.isReallyEmpty == true){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_input_password, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            return
        } else if (Constant.isValidEmail(testStr: txtUsername.text!) == false) {
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_wrong_email_format, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            return
        } else{
            callAPILogin(username: txtUsername.text!, password: txtPassword.text!)
        }
    }
    
    func callAPILogin(username: String, password: String){
            let param = HTTPBody.loginBody(username: username, password: password)
            print("param: \(String(describing: param))")
            
            let url = NSURL(string:Constant.getAPILogin())
            print("LOGIN URL:\(String(describing: url!))")
            Alamofire.request(Constant.getAPILogin(), method: .post, parameters: param, encoding: JSONEncoding.default, headers: ConnectionManager.sharedIns.getHTTPHeadersSAMDB())
                .responseObject{ (response: DataResponse<LoginModel>) in
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil {
                            self.loginModel = response.result.value
                            let statusCode = response.response?.statusCode
                            print("statusCode:\(String(describing: statusCode))")
                            if (statusCode == 200){
                                let datas = self.loginModel?.data
                                if ((datas?.count)! > 0) {
                                    for i in 0..<(datas?.count)!  {
                                        let clientID = datas![i].clientID
                                        let clientEmail = datas![i].clientEmail
                                        let clientPassword = datas![i].clientPassword
                                        let clientPhone = datas![i].clientPhone
                                        CustomUserDefaults.shared.setUsername(clientEmail ?? "")
                                    CustomUserDefaults.shared.setPhoneNumber(clientPhone ?? "")
                                        CustomUserDefaults.shared.setPassword(clientPassword ?? "")
                                        CustomUserDefaults.shared.setGuestID(clientID ?? "")
                                    }
                                    self.homePage()
                                }
                            } else {
                                let status = self.loginModel?.message
                                AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: status!, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                            }
                        }
                    case .failure(_):
                        AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed to login", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                    }
            }
        }
        
        func homePage(){
            let storyboard = UIStoryboard(name: Constant.sharedIns.MAIN_STORYBOARD, bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.MAINMENU_NAV)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
}

