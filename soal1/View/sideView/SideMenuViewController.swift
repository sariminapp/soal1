//
//  SideMenuViewController.swift
//  soal1
//
//  Created by Oscar Perdanakusuma Adipati on 27/12/20.
//

import Foundation
import UIKit
import SideMenu
import Alamofire
import AlamofireObjectMapper
import Kingfisher

class SideMenuViewController: UIViewController {
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblJabatan: UILabel!
    @IBOutlet weak var lblTglLahir: UILabel!
    @IBOutlet weak var logoutView: UIView!
    var pokemonListModel : PokemonListModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        addTapEventForView(view: logoutView, action: #selector(logoutClicked))
        if(isConnectingInternet()==false){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_internet_error, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            return
        } else {
            callAPIProfile()
        }
    }
    
    func callAPIProfile() {
        Alamofire.request(Constant.getAPIPokemonLimit(), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ConnectionManager.sharedIns.getHTTPHeadersSAMDB())
            .responseObject{ (response: DataResponse<PokemonListModel>) in
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil {
                        self.pokemonListModel = response.result.value
                        let statusCode = response.response?.statusCode
                        print("statusCode:\(String(describing: statusCode))")
                        if (statusCode == 200){
                            let datas = self.pokemonListModel?.results
                            if ((datas?.count)! > 0) {
                                DispatchQueue.main.async {
                                    let url = URL(string: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/15.png")
                                    self.imgProfile?.kf.setImage(with: url)
                                    self.imgProfile.contentMode = .scaleToFill
                                    self.lblName.text = "Nama Orang"
                                    self.lblName.textColor = .black
                                    self.lblJabatan.text = "Jabatan"
                                    self.lblJabatan.textColor = .black
                                    self.lblTglLahir.text = "Tanggal Lahir"
                                    self.lblTglLahir.textColor = .black
                                }
                            }
                        } else {
                            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get Profile data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                        }
                    }
                case .failure(_):
                    AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get Profile data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                }
        }
    }
    
    @objc func logoutClicked () {
        if(isConnectingInternet()==false){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_internet_error, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            return
        }else{
            AppController.sharedInstance.logout()
        }
    }
}
