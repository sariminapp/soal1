//
//  MainMenuViewController.swift
//  soal1
//
//  Created by Oscar Perdanakusuma Adipati on 27/12/20.
//

import UIKit
import SideMenu
import Alamofire
import AlamofireObjectMapper
import Kingfisher

class MainMenuViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    var pokemonListModel : PokemonListModel?
    var pokemonImages = [String]()
    var frame = CGRect.zero

    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        if(isConnectingInternet()==false){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_internet_error, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            return
        } else {
            setupSideMenu()
            callAPIProfile()
        }
    }

    func callAPIProfile() {
        removeAll()
        Alamofire.request(Constant.getAPIPokemonLimit(), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ConnectionManager.sharedIns.getHTTPHeadersSAMDB())
            .responseObject{ (response: DataResponse<PokemonListModel>) in
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil {
                        self.pokemonListModel = response.result.value
                        let statusCode = response.response?.statusCode
                        print("statusCode:\(String(describing: statusCode))")
                        if (statusCode == 200){
                            let datas = self.pokemonListModel?.results
                            if ((datas?.count)! > 0) {
                                for i in 0..<(datas?.count)!  {
                                    let name = datas![i].name
                                    let url = datas![i].url
                                    let images = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(i + 1).png"
                                    self.pokemonImages.append(images)
                                }
                                DispatchQueue.main.async {
                                    let url = URL(string: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/15.png")
                                    self.imgProfile?.kf.setImage(with: url)
                                    self.imgProfile.contentMode = .scaleToFill
                                    self.pageControl.numberOfPages = self.pokemonImages.count
                                    self.setupScreens()
                                }
                            }
                        } else {
                            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get Profile data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                        }
                    }
                case .failure(_):
                    AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Failed get Profile data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                }
        }
    }
    
    func removeAll() {
        pokemonImages.removeAll()
    }
    
    func setupScreens() {
        for index in 0..<pokemonImages.count {
            // 1.
            frame.origin.x = scrollView.frame.size.width * CGFloat(index)
            frame.size = scrollView.frame.size
            
            // 2.
            let imgView = UIImageView(frame: frame)
            let url = URL(string: pokemonImages[index])
            imgView.kf.setImage(with: url)
            imgView.contentMode = .scaleToFill

            self.scrollView.addSubview(imgView)
        }

        // 3.
        scrollView.contentSize = CGSize(width: (scrollView.frame.size.width * CGFloat(pokemonImages.count)), height: scrollView.frame.size.height)
        scrollView.delegate = self
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
        pageControl.currentPage = Int(pageNumber)
    }
    
    private func setupSideMenu() {
        // Define the menus
        SideMenuManager.default.leftMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? SideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        SideMenuManager.default.addPanGestureToPresent(toView: navigationController!.navigationBar)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view)
    }
}

extension MainMenuViewController: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appearing! (animated: \(animated))")
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appeared! (animated: \(animated))")
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappearing! (animated: \(animated))")
    }
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappeared! (animated: \(animated))")
//        dismiss(animated: true, completion: nil)
    }
}
