//
//  MovieModel.swift
//  movie
//
//  Created by Oscar Perdanakusuma Adipati on 29/06/20.
//  Copyright © 2020 Sarimin App. All rights reserved.
//

import ObjectMapper
import Foundation

class PokemonListModel : NSObject, Mappable{
    
    var count: Int?
    var results: [resultsData]?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        results <- map["results"]
        count <- map["count"]
    }
    
}

class resultsData : NSObject, Mappable{
    
    var name, url: String?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        name <- map["name"]
        url <- map["url"]
    }
    
}

